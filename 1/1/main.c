//
//  main.c
//  1
//
//  Created by Ryutaro Ishii on 2015/11/30.
//  Copyright © 2015年 Ryutaro Ishii. All rights reserved.
//

#include <stdio.h>
#include <math.h>

void print_halfed(double quantities[100],double halfed_time,int N){
    int i;
    double t = 0;
    for(i=0;i<N;i++){
        t = t + 1/halfed_time;
        printf("\t%lf\t%lf\n",t,quantities[i]);}
    printf("\n");}

double halfed_every(double now_quantity,int now_time){
    double next_quantity;
    next_quantity = now_quantity - log(2) * now_quantity * now_time;
    return next_quantity;
}

double halfed(double halfed_time){
    double t = 0;
    double quantity[100];
    double changed_cell[100];
    int N;
    int i;
    quantity[0] = 1;
    changed_cell[0] = 0;
    for(i=0;;i++){
        t = t + 1/halfed_time;
        quantity[i+1] = quantity[i] - log(2) * quantity[i] * t;
        changed_cell[i+1] = changed_cell[i] + quantity[i] - quantity[i+1];
        changed_cell[i+1] = halfed_every(changed_cell[i+1],t);
        if(quantity[i+1] < 0 && changed_cell[i+1] < 0) break;
    }
    N = i; t = 0;
    //print_halfed(quantity,halfed_time,N);
    print_halfed(changed_cell,halfed_time,N);
    return 1;
}

int main(int argc, const char * argv[]){
    halfed(14.8);
    // something updated
}
